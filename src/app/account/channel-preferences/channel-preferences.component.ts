import {Component, Input} from '@angular/core';
import {Channel} from 'src/app/shared/interfaces/channel';
import {ChannelPreferencesService} from 'src/app/shared/services/channel-preferences-service/channel-preferences.service';

@Component({
  selector: 'app-channel-preferences',
  templateUrl: './channel-preferences.component.html',
  styleUrls: ['./channel-preferences.component.scss']
})
export class ChannelPreferencesComponent {
  @Input() channel: Channel;

  constructor(private channelPreferencesService: ChannelPreferencesService) {
  }

}
