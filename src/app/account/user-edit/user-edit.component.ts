import {UserService} from '../../shared/services/user-service/user.service';
import {AuthService} from '../../shared/services/auth-service/auth.service';
import {Component, OnInit} from '@angular/core';
import {Channel} from 'src/app/shared/interfaces/channel';
import {ChannelPreferencesService} from '../../shared/services/channel-preferences-service/channel-preferences.service';
import {User} from '../../shared/interfaces/user';
import {NotificationService} from '../../shared/services/notification-service/notification.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
  providers: []
})
export class UserEditComponent implements OnInit {


  channels: Channel[];
  user: User;
  public areaIsDisabled: boolean;
  public usernameDisabled: boolean;
  usernameField: string;

  constructor(private channelPreferencesService: ChannelPreferencesService,
              private authService: AuthService,
              private userService: UserService,
              private notificationService: NotificationService) {
  }

  ngOnInit(): void {
    this.areaIsDisabled = true;
    this.usernameDisabled = true;
    this.user = this.authService.currentUser;
    this.authService.userStatusChanged.subscribe(() => {
      this.user = this.authService.currentUser;
    });
  }

  public updateUserName(): void {
    if (this.usernameField) {
      this.user.displayName = this.usernameField;
      this.updateUserInBackend();
    } else {
      this.notificationService.showWarning('Bitte geben sie einen neuen Usernamen ein!', 'Achtung');
    }
  }

  public updateAboutMe(): void {
    this.updateUserInBackend();
  }

  public onSaveAvatar(id: number): void {
    this.user.avatarId = id;
    this.userService.updateUser(this.user).subscribe(user => {
      this.authService.updateUserOnChanges(user);
      this.notificationService.showSuccess('Ihre Änderungen wurden erfolgreich übernommen!', 'Super!');
    });
  }

  private updateUserInBackend(): void {
    this.userService.updateUser(this.user).subscribe(user => {
      this.authService.updateUserOnChanges(user);
      this.notificationService.showSuccess('Ihre Änderungen wurden erfolgreich übernommen!', 'Super!');
    });
  }
}

