import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AccountRoutingModule} from './account-routing.module';
import {ChannelPreferencesComponent} from './channel-preferences/channel-preferences.component';
import {UserEditComponent} from './user-edit/user-edit.component';


@NgModule({
  declarations: [ChannelPreferencesComponent, UserEditComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AccountModule {
}
