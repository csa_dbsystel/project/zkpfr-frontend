import {ChannelPreferencesComponent} from './channel-preferences/channel-preferences.component';
import {UserEditComponent} from './user-edit/user-edit.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../shared/guards/auth-guard/auth.guard';


const routes: Routes = [
  {
    path: 'account',
    component: UserEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'account:userId',
    component: UserEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'channel-preferences',
    component: ChannelPreferencesComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {
}
