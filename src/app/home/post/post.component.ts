import {AuthService} from '../../shared/services/auth-service/auth.service';
import {User} from '../../shared/interfaces/user';
import {HomeService} from '../services/home.service';
import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Reply} from '../../shared/interfaces/reply';
import {faPaperPlane} from '@fortawesome/free-solid-svg-icons';
import {NotificationService} from '../../shared/services/notification-service/notification.service';
import {Post} from '../../shared/interfaces/post';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit, OnDestroy {

  faPaperPlane = faPaperPlane;
  isLoading: boolean;
  replyForm: FormGroup;
  currentPostId: number;
  allRepliesFromCurrentPost: Reply[];
  currentPost: Post;
  postHasReplies: boolean;
  newReply = {} as Reply;
  currentUser: User;
  postRefreshTimer;
  @ViewChild('chatHeader', {static: false}) chatHeaderElement: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private homeService: HomeService,
    private authService: AuthService,
    private router: Router,
    private notificationService: NotificationService,
    private spinner: NgxSpinnerService,
  ) {
  }

  private _chatBodyHeight: string;

  get chatBodyHeight(): string {
    return this._chatBodyHeight;
  }

  set chatBodyHeight(value: string) {
    this._chatBodyHeight = value;
  }

  get postIsClosed(): boolean {
    return this.currentPost?.status.id === 2;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.chatBodyHeight = this.calculateHeight();
  }

  public ngOnInit(): void {
    this.initialization();
  }

  public ngOnDestroy(): void {
    clearInterval(this.postRefreshTimer);
  }

  public getAllRepliesFromCurrentPost(): void {
    this.homeService.getAllRepliesFromCurrentPost(this.currentPostId).subscribe((allReplies: Reply[]) => {
      this.allRepliesFromCurrentPost = allReplies;
      this.postHasReplies = !!this.allRepliesFromCurrentPost.length;
      if (this.isLoading) {
        this.spinner.hide()
          .then(() => {
            this.isLoading = false;
            this.chatBodyHeight = this.calculateHeight();
          });
      }
    });
  }

  public getCurrentPost(): void {
    this.homeService.getPostById(this.currentPostId).subscribe((post: Post) => {
      this.currentPost = post;
    });
  }

  public onNewReplySubmit(): void {
    if (this.replyForm.valid) {
      this.newReply.post = this.currentPost;
      this.newReply.replier = this.currentUser;
      this.newReply.text = this.replyForm.get('replyText').value;
      this.newReply.likes = 0;
      this.newReply.creationDate = new Date().toISOString().slice(0, 19);
      this.homeService.submitReply(this.newReply).subscribe((newReply: Reply) => {
        this.getAllRepliesFromCurrentPost();
        this.replyForm.reset();
      });
    }
  }

  public onPostClose(): void {
    this.currentPost.status = {
      id: 2,
      name: 'geschlossen',
      short: 'CLOSED'
    };
    this.homeService.submitPostOrChanges(this.currentPost).subscribe((changedPost: Post) => {
      this.router.navigateByUrl('/')
        .then(() => {
          this.notificationService.showWarning('Ihr Post wurde erfolgreich geschlossen!', 'Achtung');
        });
    });
  }

  public canClosePost(): boolean {
    const isOwnPost: boolean = this.currentPost?.user.id === this.currentUser.id;
    const postIsOpen: boolean = this.currentPost?.status.id === 1;
    return isOwnPost && postIsOpen;
  }

  private initialization(): void {
    this.spinner.show()
      .then(() => {
        this.isLoading = true;
      });
    this.postHasReplies = false;
    this.initializeFormControl();
    this.currentPostId = +this.route.snapshot.paramMap.get('id');
    this.getCurrentUser();
    this.getCurrentPost();
    this.getAllRepliesFromCurrentPost();
    this.postRefreshTimer = setInterval(() => this.getAllRepliesFromCurrentPost(), 5000);
  }

  private initializeFormControl() {
    this.replyForm = new FormGroup({
      replyText: new FormControl('', [
        Validators.required
      ])
    });
  }

  private getCurrentUser(): void {
    this.currentUser = this.authService.currentUser;
  }

  private calculateHeight(): string {
    const heightOfHeaderAndInput = 67 + 38;
    const heightForChat = window.innerHeight - heightOfHeaderAndInput - this.chatHeaderElement.nativeElement.offsetHeight;
    return heightForChat + 'px';
  }
}
