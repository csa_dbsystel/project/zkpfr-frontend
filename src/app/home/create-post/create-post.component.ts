import {AuthService} from '../../shared/services/auth-service/auth.service';
import {User} from '../../shared/interfaces/user';
import {HomeService} from '../services/home.service';
import {Component, OnInit} from '@angular/core';
import {Post} from '../../shared/interfaces/post';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../shared/services/notification-service/notification.service';
import {Channel} from '../../shared/interfaces/channel';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})

export class CreatePostComponent implements OnInit {
  allChannels: Channel[];
  newPost = {} as Post;
  newPostTitle: string;
  newPostBody: string;
  currentChannelId: number;
  currentChannelIndex: number;
  currentUser: User;

  constructor(
    private route: ActivatedRoute,
    private homeService: HomeService,
    private authService: AuthService,
    private router: Router,
    private notificationService: NotificationService
  ) {
  }


  ngOnInit(): void {
    this.initialization();
  }

  public onNewPostSubmit(): void {
    if (this.newPostTitle === '' || this.newPostTitle === undefined || this.newPostBody === '' || this.newPostBody === undefined) {
      this.notificationService.showError('Bitte füllen sie alle Felder aus!', 'Fehler!');
      return;
    }
    this.newPost.creationDate = new Date().toISOString().slice(0, 19);
    this.newPost.user = this.currentUser;
    this.newPost.title = this.newPostTitle;
    this.newPost.body = this.newPostBody;
    this.newPost.channel = this.allChannels[this.currentChannelIndex];
    this.newPost.status = {
      id: 1,
      name: 'testing',
      short: 'TEST'
    };
    this.homeService.submitPostOrChanges(this.newPost).toPromise()
      .catch((err) => {
        console.log(err.message);
      });
    this.newPostTitle = '';
    this.newPostBody = '';
    this.router.navigate([''])
      .then(() => {
        this.notificationService.showSuccess('Ihr Post wurde erstellt!', 'Super!');
      });
  }

  private initialization(): void {
    this.currentUser = this.authService.currentUser;
    this.currentChannelId = +this.route.snapshot.paramMap.get('id');
    this.getAllChannels();
  }

  private getAllChannels(): void {
    this.homeService.getAllChannels().subscribe((allChannels: Channel[]) => {
      this.allChannels = allChannels;
      this.allChannels.forEach((channel, index) => {
        if (channel.id === this.currentChannelId) {
          this.currentChannelIndex = index;
        }
      });
    });
  }
}
