import {Post} from '../../shared/interfaces/post';
import {Reply} from '../../shared/interfaces/reply';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Channel} from '../../shared/interfaces/channel';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(
    private http: HttpClient
  ) {
  }

  public getAllChannels(): Observable<Channel[]> {
    return this.http.get<Channel[]>(environment.rootUrl + '/channels');
  }

  public getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(environment.rootUrl + '/posts');
  }

  public getAllRepliesFromCurrentPost(postId: number): Observable<Reply[]> {
    return this.http.get<Reply[]>(environment.rootUrl + '/replies/posts/' + postId);
  }

  public getPostById(id: number): Observable<Post> {
    return this.http.get<Post>(environment.rootUrl + '/posts/' + id);
  }

  public submitReply(newReply: Reply): Observable<Reply> {
    return this.http.post<Reply>(environment.rootUrl + '/replies', newReply);
  }

  public submitPostOrChanges(newPost: Post): Observable<Post> {
    return this.http.post<Post>(environment.rootUrl + '/posts', newPost);
  }
}
