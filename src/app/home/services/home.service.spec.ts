import {TestBed} from '@angular/core/testing';
import {HomeService} from './home.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Post} from '../../shared/interfaces/post';
import {Reply} from '../../shared/interfaces/reply';
import {environment} from '../../../environments/environment';
import {User} from '../../shared/interfaces/user';
import {Channel} from '../../shared/interfaces/channel';
import {Status} from '../../shared/interfaces/status';

describe('HomeService UnitTest', () => {
  let service: HomeService;
  let httpTestingController: HttpTestingController;
  let postMock: Post[];
  let channelMock: Channel[];
  let replyMock: Reply[];
  let singlePostMock: Post;
  let singleReplyMock: Reply;
  const dummyUser: User = {
    aboutMe: '',
    activated: 0,
    creationDate: '',
    displayName: 'Maxetestuser',
    email: 'test@example.com',
    id: 0,
    lastAccessDate: '',
    reputation: 0,
    avatarId: 0
  };
  const dummyChannel: Channel = {
    id: 1,
    name: 'lost and found'
  };
  const dummyStatus: Status = {
    id: 1,
    name: 'status',
    short: 'st'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HomeService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send a GET request on getAllChannels and return all mocked channels', () => {
    channelMock = [{id: 1, name: 'Lost and Found'}, {id: 2, name: 'Reiseinformation'}];
    service.getAllChannels().subscribe((channels: Channel[]) => {
      expect(channels).toEqual(channelMock);
    });
    const req = httpTestingController.expectOne(environment.rootUrl + '/channels');
    expect(req.request.method).toBe('GET');
    req.flush(channelMock);
  });

  it('should send a GET request on getAllPosts and return all mocked posts', () => {
    postMock = [{
      id: 1,
      creationDate: 'example',
      viewCount: 2,
      user: dummyUser,
      title: 'lonely',
      channel: dummyChannel,
      body: 'test',
      status: dummyStatus
    }, {
      id: 2,
      creationDate: 'asdas',
      viewCount: 2,
      user: dummyUser,
      title: 'lost',
      channel: dummyChannel,
      body: 'test2',
      status: dummyStatus
    }];
    service.getAllPosts().subscribe((posts: Post[]) => {
      expect(posts).toEqual(postMock);
    });
    const req = httpTestingController.expectOne(environment.rootUrl + '/posts');
    expect(req.request.method).toBe('GET');
    req.flush(postMock);
  });

  it('should send a GET request on getAllReplies and return all Replies', () => {
    replyMock = [{
      id: 1,
      post: null,
      replier: dummyUser,
      text: 'Hi',
      likes: 12,
      creationDate: 'somewhere'
    }, {
      id: 2,
      post: null,
      replier: dummyUser,
      text: 'Hallo da',
      likes: 12,
      creationDate: 'somewhere'
    }];
    const postId = 1;
    service.getAllRepliesFromCurrentPost(postId).subscribe((replies: Reply[]) => {
      expect(replies).toEqual(replyMock);
    });
    const req = httpTestingController.expectOne(environment.rootUrl + '/replies/posts/' + postId);
    expect(req.request.method).toEqual('GET');
    req.flush(replyMock);
  });

  it('should send a POST request on submitReply and return the submitted reply', () => {
    singleReplyMock = {
      id: 1,
      post: null,
      replier: dummyUser,
      text: 'Meine Nachricht',
      likes: 12,
      creationDate: 'sometime'
    };
    service.submitReply(singleReplyMock).subscribe((submittedReply: Reply) => {
      expect(submittedReply).toEqual(singleReplyMock);
    });
    const req = httpTestingController.expectOne(environment.rootUrl + '/replies');
    expect(req.request.method).toEqual('POST');
    req.flush(singleReplyMock);
  });

  it('should send a POST request on submitPost and return the submitted post', () => {
    singlePostMock = {
      body: 'Test',
      channel: undefined,
      creationDate: 'sometime',
      id: 1,
      status: undefined,
      title: 'mock',
      user: undefined,
      viewCount: 2
    };
    service.submitPostOrChanges(singlePostMock).subscribe((submittedPost: Post) => {
      expect(submittedPost).toEqual(singlePostMock);
    });
    const req = httpTestingController.expectOne(environment.rootUrl + '/posts');
    expect(req.request.method).toEqual('POST');
    req.flush(singlePostMock);
  });

  afterEach(() => {
    localStorage.clear();
    httpTestingController.verify();
  });
});
