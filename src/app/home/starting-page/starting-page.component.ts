import {User} from '../../shared/interfaces/user';
import {AuthService} from '../../shared/services/auth-service/auth.service';
import {HomeService} from '../services/home.service';
import {Component, OnInit} from '@angular/core';
import {Channel} from '../../shared/interfaces/channel';
import {Post} from '../../shared/interfaces/post';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-starting-page',
  templateUrl: './starting-page.component.html',
  styleUrls: ['./starting-page.component.scss']
})

export class StartingPageComponent implements OnInit {
  allChannels: Channel[];
  allPosts: Post [];
  refreshed: boolean;
  currentUser: User;
  isLoading: boolean;

  constructor(
    private homeService: HomeService,
    private authService: AuthService,
    private spinnerService: NgxSpinnerService
  ) {
  }

  ngOnInit(): void {
    this.initialization();
  }

  private initialization(): void {
    this.spinnerService.show()
      .then(() => {
        this.isLoading = true;
      });
    this.refreshed = false;
    this.currentUser = this.authService.currentUser;
    this.fetchData()
      .then((responseData: [Channel[], Post[]]) => {
        this.allChannels = responseData[0];
        this.allPosts = responseData[1];
        this.spinnerService.hide()
          .then(() => {
            this.isLoading = false;
          });
      });
    this.refreshed = true;
  }

  private getAllChannels(): Promise<Channel[]> {
    return this.homeService.getAllChannels().toPromise();
  }

  private getAllPosts(): Promise<Post[]> {
    return this.homeService.getAllPosts().toPromise();
  }

  private async fetchData(): Promise<[Channel[], Post[]]> {
    const allChannelsResolved = await this.getAllChannels();
    const allPostsResolved = await this.getAllPosts();
    return Promise.all([allChannelsResolved, allPostsResolved]);
  }
}
