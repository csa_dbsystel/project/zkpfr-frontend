import {CreatePostComponent} from './create-post/create-post.component';
import {StartingPageComponent} from './starting-page/starting-page.component';
import {PostComponent} from './post/post.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../shared/guards/auth-guard/auth.guard';


const routes: Routes = [
  {path: '', component: StartingPageComponent, canActivate: [AuthGuard]},
  {path: 'post/:id', component: PostComponent, canActivate: [AuthGuard]},
  {path: 'createPost/:id', component: CreatePostComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
