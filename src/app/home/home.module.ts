import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeRoutingModule} from './home-routing.module';
import {PostComponent} from './post/post.component';
import {StartingPageComponent} from './starting-page/starting-page.component';
import {CreatePostComponent} from './create-post/create-post.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BrowserModule} from '@angular/platform-browser';
import {NgxSpinnerModule} from 'ngx-spinner';


@NgModule({
  declarations: [PostComponent, StartingPageComponent, CreatePostComponent],
  imports: [
    BrowserModule,
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    NgxSpinnerModule
  ]
})
export class HomeModule {
}
