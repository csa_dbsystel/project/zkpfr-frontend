import {Component, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {HeaderComponent} from './header/header.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public title = 'zkpfr-frontend';

  constructor(private router: Router) {
  }

  @ViewChild(HeaderComponent, {static: true}) private _header: HeaderComponent;

  get header(): HeaderComponent {
    return this._header;
  }

  ngOnInit(): void {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.onNavigation();
    });
  }

  onNavigation(): void {
    window.scroll(0, 0);
    if (this.header.navIsOpen) {
      this.header.toggleSidenav();
    }
  }
}
