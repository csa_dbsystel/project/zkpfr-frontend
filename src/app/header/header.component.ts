import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth-service/auth.service';
import {Router} from '@angular/router';
import {faBars} from '@fortawesome/free-solid-svg-icons';
import {NotificationService} from '../shared/services/notification-service/notification.service';
import {User} from '../shared/interfaces/user';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  faBars = faBars;

  constructor(private _authService: AuthService,
              private _router: Router,
              private notificationService: NotificationService) {
  }

  private _userIsLoggedIn: boolean;

  get userIsLoggedIn(): boolean {
    return this._userIsLoggedIn;
  }

  set userIsLoggedIn(value: boolean) {
    this._userIsLoggedIn = value;
  }

  private _currentUser: User;

  get currentUser(): User {
    return this._currentUser;
  }

  set currentUser(value: User) {
    this._currentUser = value;
  }

  private _navIsOpen: boolean;

  get navIsOpen(): boolean {
    return this._navIsOpen;
  }

  set navIsOpen(value: boolean) {
    this._navIsOpen = value;
  }

  get authService(): AuthService {
    return this._authService;
  }

  get router(): Router {
    return this._router;
  }

  ngOnInit(): void {
    this.navIsOpen = false;
    this.initializeAuthStatus();
  }

  public openSidenav(): void {
    const sidenavElRef = document.getElementById('customSideNav');
    setTimeout(() => {
      sidenavElRef.style.width = '250px';
    }, 500);
    sidenavElRef.classList.add('sidenav', 'show');
    this.navIsOpen = true;
  }

  public closeSidenav(): void {
    const sidenavElRef = document.getElementById('customSideNav');
    setTimeout(() => {
      sidenavElRef.classList.remove('sidenav', 'show');
    }, 500);
    sidenavElRef.style.width = '0px';
    this.navIsOpen = false;
  }

  public toggleSidenav(): void {
    if (this.navIsOpen) {
      this.closeSidenav();
      this.navIsOpen = false;
    } else {
      this.openSidenav();
      this.navIsOpen = true;
    }
  }

  public performLogout(): void {
    this.authService.logout();
    this.router.navigate(['signin'])
      .then(() => {
        this.notificationService.showSuccess('Sie wurden erfolgreich ausgeloggt!', 'Bis zum nächsten mal!');
      });
  }

  private initializeAuthStatus() {
    this.userIsLoggedIn = this.authService.isLoggedIn;
    this.currentUser = this.authService.currentUser;
    this.authService.userStatusChanged.subscribe(() => {
      this.userIsLoggedIn = this.authService.isLoggedIn;
      this.currentUser = this.authService.currentUser;
    });
  }
}
