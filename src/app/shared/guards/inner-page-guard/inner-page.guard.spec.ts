import {InnerPageGuard} from './inner-page.guard';
import {AuthService} from '../../services/auth-service/auth.service';
import {TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('InnerPageGuard UnitTest', () => {
  let guard: InnerPageGuard;
  let authServiceMock: AuthService;
  const routeMock: any = {snapshot: {}};
  const routerStateMock: any = {snapshot: {}, url: '/signin'};
  const routerMock = {navigate: jasmine.createSpy('navigate')};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InnerPageGuard, {provide: Router, useValue: routerMock}],
      imports: [HttpClientTestingModule]
    });
    authServiceMock = TestBed.inject(AuthService);
    guard = TestBed.inject(InnerPageGuard);
  });

  it('should redirect an authenticated user to the home route', () => {
    spyOnProperty(authServiceMock, 'isLoggedIn').and.returnValue(true);
    expect(guard.canActivate(routeMock, routerStateMock)).toEqual(false);
    expect(routerMock.navigate).toHaveBeenCalledWith(['']);
  });

  it('should allow an unauthenticated user to acces auth routes', () => {
    expect(guard.canActivate(routeMock, routerStateMock)).toEqual(true);
  });
});
