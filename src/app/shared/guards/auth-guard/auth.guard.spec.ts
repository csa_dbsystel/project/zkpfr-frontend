import {AuthGuard} from './auth.guard';
import {AuthService} from '../../services/auth-service/auth.service';
import {TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('AuthGuard UnitTest', () => {
  let guard: AuthGuard;
  let authServiceMock: AuthService;
  const routeMock: any = {snapshot: {}};
  const routerStateMock: any = {snapshot: {}, url: '/account'};
  const routerMock = {navigate: jasmine.createSpy('navigate')};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, {provide: Router, useValue: routerMock}],
      imports: [HttpClientTestingModule]
    });
    authServiceMock = TestBed.inject(AuthService);
    guard = TestBed.inject(AuthGuard);
  });

  it('should redirect an unauthenticated user to the login route', () => {
    expect(guard.canActivate(routeMock, routerStateMock)).toEqual(false);
    expect(routerMock.navigate).toHaveBeenCalledWith(['signin']);
  });

  it('should allow an authenticated user to access protected routes', () => {
    spyOnProperty(authServiceMock, 'isLoggedIn').and.returnValue(true);
    expect(guard.canActivate(routeMock, routerStateMock)).toEqual(true);
  });
});
