export interface User {
  id: number;
  creationDate: string;
  lastAccessDate: string;
  displayName: string;
  reputation: number;
  email: string;
  aboutMe: string;
  activated: number;
  avatarId: number;
}
