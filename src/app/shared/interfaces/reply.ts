import {Post} from './post';
import {User} from './user';

export interface Reply {
  id: number;
  post: Post;
  replier: User;
  text: string;
  likes: number;
  creationDate: string;
}
