import {User} from './user';
import {Channel} from './channel';
import {Status} from './status';

export interface Post {
  id: number;
  creationDate: string;
  viewCount: number;
  user: User;
  title: string;
  body: string;
  channel: Channel;
  status: Status;
}
