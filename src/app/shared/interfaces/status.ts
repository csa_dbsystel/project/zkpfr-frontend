export interface Status {
  id: number;
  name: string;
  short: string;
}
