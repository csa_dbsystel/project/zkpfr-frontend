import {AbstractControl} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {debounceTime, map} from 'rxjs/operators';

export class EmailValidator {
  static validateEmail(httpClient: HttpClient) {
    return (control: AbstractControl) => {
      const emailToCheck = control.value.toLowerCase();
      return httpClient.get<boolean>(environment.rootUrl + '/auth/validateEmail/' + emailToCheck)
        .pipe(
          debounceTime(500),
          map(isAvailable => !isAvailable ? {emailAvailable: false} : null)
        );
    };
  }
}
