import {HttpClient} from '@angular/common/http';
import {AbstractControl} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {debounceTime, map} from 'rxjs/operators';

export class DisplayNameValidator {
  static displayNameIsValid(httpClient: HttpClient) {
    return (control: AbstractControl) => {
      const displayNameToCheck = control.value;
      return httpClient.get<boolean>(environment.rootUrl + '/auth/validateUsername/' + displayNameToCheck)
        .pipe(
          debounceTime(500),
          map(isAvailable => !isAvailable ? {usernameIsAvailable: false} : null)
        );
    };
  }
}
