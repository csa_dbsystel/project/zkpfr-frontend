import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../interfaces/user';
import {map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userStatusChanged: EventEmitter<void> = new EventEmitter<void>();

  constructor(private httpClient: HttpClient) {
    if (localStorage.getItem('User')) {
      this.currentUser = JSON.parse(localStorage.getItem('User'));
    } else if (sessionStorage.getItem('User')) {
      this.currentUser = JSON.parse(sessionStorage.getItem('User'));
    } else {
      this.currentUser = null;
    }
  }

  private _currentUser: User;

  get currentUser(): User | undefined {
    return this._currentUser !== null ? this._currentUser : undefined;
  }

  set currentUser(value: User) {
    this._currentUser = value;
  }

  get isLoggedIn(): boolean {
    return (localStorage.getItem('User') || sessionStorage.getItem('User')) !== null;
  }

  public updateUserOnChanges(changedUser: User): boolean {
    if (localStorage.getItem('User')) {
      localStorage.setItem('User', JSON.stringify(changedUser));
      this.currentUser = JSON.parse(localStorage.getItem('User'));
      this.userStatusChanged.emit();
      return true;
    } else if (sessionStorage.getItem('User')) {
      sessionStorage.setItem('User', JSON.stringify(changedUser));
      this.currentUser = JSON.parse(sessionStorage.getItem('User'));
      this.userStatusChanged.emit();
      return true;
    } else {
      return false;
    }
  }

  async login(reqEmail: string, reqPassword: string, rememberMe: boolean): Promise<User> {
    const reqBody = {
      email: reqEmail.toLowerCase(),
      password: reqPassword
    };
    return await this.httpClient.post<User>(environment.rootUrl + '/auth/login', reqBody)
      .pipe(
        map(receivedUser => {
          this.cacheUser(rememberMe, receivedUser);
          return receivedUser;
        })
      ).toPromise();
  }

  async signUp(reqEmail: string, reqPassword: string, reqDisplayName: string, rememberMe: boolean): Promise<User> {
    const reqBody = {
      email: reqEmail.toLowerCase(),
      password: reqPassword,
      displayName: reqDisplayName
    };
    return await this.httpClient.post<User>(environment.rootUrl + '/auth/register', reqBody)
      .pipe(
        map(registeredUser => {
          this.cacheUser(rememberMe, registeredUser);
          return registeredUser;
        })
      ).toPromise();
  }

  public logout(): void {
    if (localStorage.getItem('User')) {
      localStorage.removeItem('User');
    } else if (sessionStorage.getItem('User')) {
      sessionStorage.removeItem('User');
    }
    this.userStatusChanged.emit();
  }

  private cacheUser(rememberMe: boolean, user: User): void {
    if (rememberMe) {
      localStorage.setItem('User', JSON.stringify(user));
    } else {
      sessionStorage.setItem('User', JSON.stringify(user));
    }
    this.currentUser = user;
    this.userStatusChanged.emit();
  }
}
