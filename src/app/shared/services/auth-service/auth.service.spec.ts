import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {User} from '../../interfaces/user';
import {environment} from '../../../../environments/environment';
import {TestBed} from '@angular/core/testing';
import {AuthService} from './auth.service';

describe('AuthService UnitTest', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;
  let store = {};
  let setSpy: jasmine.Spy;
  let removeSpy: jasmine.Spy;
  const dummyUser: User = {
    aboutMe: '',
    activated: 0,
    creationDate: '',
    displayName: 'Maxetestuser',
    email: 'test@example.com',
    id: 0,
    lastAccessDate: '',
    reputation: 0,
    avatarId: 0
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);

    spyOn(localStorage, 'getItem').and.callFake((key: string): string => {
      return store[key] || null;
    });
    removeSpy = spyOn(localStorage, 'removeItem').and.callFake((key: string): void => {
      delete store[key];
    });
    setSpy = spyOn(localStorage, 'setItem').and.callFake((key: string, value: string): string => {
      return store[key] = value;
    });
    spyOn(localStorage, 'clear').and.callFake(() => {
      store = {};
    });
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send a POST request on login and return a valid User', () => {
    service.login('test@example.com', '123', false)
      .then((user) => {
        expect(user).toEqual(dummyUser);
        expect(service.currentUser).toEqual(dummyUser);
        expect(service.isLoggedIn).toBeTrue();
      });
    const req = httpMock.expectOne(environment.rootUrl + '/auth/login');
    expect(req.request.method).toBe('POST');
    req.flush(dummyUser);
  });

  it('should send a POST request on signup and return a valid User', () => {
    service.signUp('test@example.com', '123', 'Maxetestuser', true)
      .then((user) => {
        expect(user).toEqual(dummyUser);
        expect(service.currentUser).toEqual(dummyUser);
        expect(service.isLoggedIn).toBeTrue();
      });
    const req = httpMock.expectOne(environment.rootUrl + '/auth/register');
    expect(req.request.method).toBe('POST');
    req.flush(dummyUser);
  });

  it('should cache user in local storage if rememberme is true', () => {
    // @ts-ignore
    service.cacheUser(true, dummyUser);
    expect(setSpy).toHaveBeenCalled();
    expect(localStorage.getItem('User')).toEqual(JSON.stringify(dummyUser));
  });

  it('should clear localstorage if user logges out when he was permanently logged in', () => {
    localStorage.setItem('User', 'someUser');
    service.logout();
    expect(removeSpy).toHaveBeenCalled();
    expect(localStorage.getItem('User')).toEqual(null);
  });

  afterEach(() => {
    localStorage.clear();
    httpMock.verify();
  });
});
