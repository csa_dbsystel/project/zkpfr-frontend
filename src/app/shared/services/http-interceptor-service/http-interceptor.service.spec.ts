import {HttpInterceptorService} from './http-interceptor.service';
import {TestBed} from '@angular/core/testing';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthService} from '../auth-service/auth.service';
import {HomeService} from '../../../home/services/home.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../../environments/environment';

describe('HttpInterceptorService Integration Test', () => {
  let authService: AuthService;
  let homeService: HomeService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        HomeService,
        {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true}
      ],
      imports: [HttpClientTestingModule]
    });
    authService = TestBed.inject(AuthService);
    homeService = TestBed.inject(HomeService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should add an authorization header for auth requests', () => {
    authService.login('email', 'password', false);

    const httpRequest = httpTestingController.expectOne(environment.rootUrl + '/auth/login');
    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
    expect(httpRequest.request.headers.get('Authorization')).toBe(environment.authorizationHeader.authorizedUserRequest.payload);
  });

  it('should not add an authorization header for unauthorized requests', () => {
    homeService.getAllPosts().subscribe();

    const httpRequest = httpTestingController.expectOne(environment.rootUrl + '/posts');
    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
    expect(httpRequest.request.headers.get('Authorization')).toBe(environment.authorizationHeader.anonymousUserRequest.payload);
  });

  it('should add an authorization header for authorized requests not in the auth module', () => {
    spyOn(localStorage, 'getItem').and.returnValue('User');

    homeService.getAllPosts().subscribe();

    const httpRequest = httpTestingController.expectOne(environment.rootUrl + '/posts');
    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
    expect(httpRequest.request.headers.get('Authorization')).toBe(environment.authorizationHeader.authorizedUserRequest.payload);
  });

  afterEach(() => {
    httpTestingController.verify();
    localStorage.clear();
    sessionStorage.clear();
  });
});
