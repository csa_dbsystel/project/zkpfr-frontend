import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  private interceptionExceptionUrls = [
    'auth/login',
    'auth/register'
  ];

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (sessionStorage.getItem('User') || localStorage.getItem('User') || this.requestIsForAuth(req.url)) {
      req = req.clone({
        setHeaders: {
          Authorization: environment.authorizationHeader.authorizedUserRequest.payload
        }
      });
    } else {
      req = req.clone({
        setHeaders: {
          Authorization: environment.authorizationHeader.anonymousUserRequest.payload
        }
      });
    }
    return next.handle(req);
  }

  private requestIsForAuth(url: string): boolean {
    const positionToCut = url.indexOf('v1/') + 3;
    const comparatorString = url.substring(positionToCut, url.length);
    return this.interceptionExceptionUrls.includes(comparatorString);
  }
}

