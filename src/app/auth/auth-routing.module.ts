import {SignUpComponent} from './sign-up/sign-up.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InnerPageGuard} from '../shared/guards/inner-page-guard/inner-page.guard';


const routes: Routes = [
  {path: 'signin', component: SignInComponent, canActivate: [InnerPageGuard]},
  {path: 'signup', component: SignUpComponent, canActivate: [InnerPageGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
