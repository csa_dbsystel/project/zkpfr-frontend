import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../shared/services/auth-service/auth.service';
import {Router} from '@angular/router';
import {NotificationService} from '../../shared/services/notification-service/notification.service';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})

export class SignInComponent implements OnInit, OnDestroy {
  constructor(private authService: AuthService,
              private router: Router,
              private notificationService: NotificationService) {
  }

  private _loginForm: FormGroup;

  get loginForm(): FormGroup {
    return this._loginForm;
  }

  set loginForm(value: FormGroup) {
    this._loginForm = value;
  }

  private _inputWasInvalid: boolean;

  get inputWasInvalid(): boolean {
    return this._inputWasInvalid;
  }

  set inputWasInvalid(value: boolean) {
    this._inputWasInvalid = value;
  }

  get emailFormField(): AbstractControl {
    return this.loginForm.get('email');
  }

  get passwordFormField(): AbstractControl {
    return this.loginForm.get('password');
  }

  get rememberMeFormField(): AbstractControl {
    return this.loginForm.get('rememberMe');
  }

  ngOnInit(): void {
    document.body.classList.add('bg-img');
    this.inputWasInvalid = false;
    this.initializeLoginFormGroup();
  }

  ngOnDestroy(): void {
    document.body.classList.remove('bg-img');
  }

  public sendLoginRequest() {
    if (this.loginForm.valid) {
      this.authService.login(this.emailFormField.value, this.passwordFormField.value, this.rememberMeFormField.value)
        .then((user) => {
          this.router.navigate([''])
            .then(() => {
              this.notificationService.showSuccess(
                'Erfolgreich Eingeloggt! Wilkommen zurück ' + user.displayName,
                'Wilkommen!');
            });
        })
        .catch((error) => {
          this.notificationService.showError('Email oder Passwort ist nicht Korrekt!', 'Fehler!');
        });
    } else {
      this.inputWasInvalid = true;
      this.notificationService.showError('Email oder Passwort ist nicht Korrekt!', 'Fehler!');
      this.loginForm.reset();
    }
  }

  private initializeLoginFormGroup() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(environment.regexForValidation.email)
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
      rememberMe: new FormControl(false)
    });
  }
}
