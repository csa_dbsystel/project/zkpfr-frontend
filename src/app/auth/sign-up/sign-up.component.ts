import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../shared/services/auth-service/auth.service';
import {Router} from '@angular/router';
import {EmailValidator} from '../../shared/validators/email-validator';
import {HttpClient} from '@angular/common/http';
import {DisplayNameValidator} from '../../shared/validators/display-name-validator';
import {NotificationService} from '../../shared/services/notification-service/notification.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit, OnDestroy {
  constructor(private authService: AuthService,
              private router: Router,
              private httpClient: HttpClient,
              private notificationService: NotificationService) {
  }

  private _registrationForm: FormGroup;

  get registrationForm(): FormGroup {
    return this._registrationForm;
  }

  set registrationForm(value: FormGroup) {
    this._registrationForm = value;
  }

  get emailFormField(): AbstractControl {
    return this.registrationForm.get('email');
  }

  get usernameFormField(): AbstractControl {
    return this.registrationForm.get('username');
  }

  get passwordFormField(): AbstractControl {
    return this.registrationForm.get('password');
  }

  get rememberMeFormField(): AbstractControl {
    return this.registrationForm.get('rememberMe');
  }

  get formControls(): { [key: string]: AbstractControl } {
    return this.registrationForm.controls;
  }

  ngOnInit(): void {
    document.body.classList.add('bg-img');
    this.initializeRegistrationForm();
  }

  ngOnDestroy(): void {
    document.body.classList.remove('bg-img');
  }

  public sendRegistrationRequest(): void {
    if (this.registrationForm.valid) {
      this.authService.signUp(
        this.emailFormField.value,
        this.passwordFormField.value,
        this.usernameFormField.value,
        this.rememberMeFormField.value)
        .then((user) => {
          this.notificationService.showSuccess(
            'Sie wurden erfolgreich registriert! Willkommen ' + user.displayName,
            'Willkommen');
          this.router.navigate(['']);
        }).catch(error => this.notificationService.showError(
        'Es ist ein Fehler aufgetreten bitte versuchen sie es später noch einmal! ' + error.message, 'Fehler!'));
    } else {
      this.notificationService.showError('Einige ihrer Eingaben sind nicht Korrekt!', 'Achtung!');
    }
  }

  private initializeRegistrationForm() {
    this.registrationForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(environment.regexForValidation.email)
      ], [
        EmailValidator.validateEmail(this.httpClient)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(environment.regexForValidation.password)
      ]),
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ], [
        DisplayNameValidator.displayNameIsValid(this.httpClient)
      ]),
      rememberMe: new FormControl(false)
    });
  }
}
