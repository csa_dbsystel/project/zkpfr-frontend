export const environment = {
  production: true,
  regexForValidation: {
    email: '^(([^<>()\\[\\]\\.,;:\\s@\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))' +
      '@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})$',
    password: '^(?=(?:[^a-z]*[a-z]){1})(?=(?:[^A-Z]*[A-Z]){1})(?=(?:[^0-9]*[0-9]){1})(?=.*[!-\/:-@\[-`{-~]).{8,}$'
  },
  authorizationHeader: {
    anonymousUserRequest: {
      payload: 'Basic ' + btoa('Anonym:w(S9<tTmC(bkPpP4)HLR}]2Y8D-fn;')
    },
    authorizedUserRequest: {
      payload: 'Basic ' + btoa('User:w(S9<tTmC(bkPpP4)HLR}]2Y8D-fn;')
    }
  },
  rootUrl: 'http://5.135.157.83:8080/api/v1'
};
