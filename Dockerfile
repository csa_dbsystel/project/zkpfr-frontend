FROM nginx:1.17.8-alpine

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

RUN rm -rf /usr/share/nginx/html/*

COPY dist/ /usr/share/nginx/html/

EXPOSE 4200 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]